$(document).ready(function(){

//    $.getJSON('http://api.aladhan.com/timingsByCity?city=Dhaka&country=BD&method=2', function(data) {
//      console.log(data.data.timings['Maghrib']);
//    });

    prayTimes.tune( {maghrib: 4.0} ); //adjust current localtime (+4 minutes) for maghrib 

    var time = prayTimes.getTimes(new Date(), [23.777176, 90.399452],'auto',0,'24h');
    var staticTime=prayTimes.getTimes(new Date(), [23.777176, 90.399452],'auto',0,'12hNS');
    maghrib = time.maghrib;
    staticMagrib = staticTime.maghrib;
    
    var x = setInterval(function() {
        var maghribArr = maghrib.split(":"); //store maghrib time (hour,minutes,seconds) in array
        var maghribHour = Number(maghribArr[0]);
        var maghribMin = Number(maghribArr[1]); 
        var magribTime= Date.today().set({hour: maghribHour, minute:maghribMin}).getTime(); // set magrib time in formatted
        var now = new Date().getTime(); //get current time
        
        var distance = magribTime - now; //get time difference from Maghrib from current time
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // convert metrices into string format
        stringHour= String(hours); 
        stringMinutes= String(minutes);
        stringSecond= String(seconds);

        //mapping for English to Bengali
        var finalEnglishToBanglaNumber={'0':'০','1':'১','2':'২','3':'৩','4':'৪','5':'৫','6':'৬','7':'৭','8':'৮','9':'৯'};
        
        String.prototype.getDigitBanglaFromEnglish = function() {
            var retStr = this;
            for (var x in finalEnglishToBanglaNumber) {
                retStr = retStr.replace(new RegExp(x, 'g'), finalEnglishToBanglaNumber[x]); //replace english number with bengali with regular expression
            }
            return retStr;
        };
        
        var convertedHour=stringHour.getDigitBanglaFromEnglish();
        var convertedMinute=stringMinutes.getDigitBanglaFromEnglish();
        var convertedSecond=stringSecond.getDigitBanglaFromEnglish();  

        var staticMaghribArr = staticMagrib.split(":");
        stringStaticHour=String(staticMaghribArr[0]);
        stringStaticMinute=String(staticMaghribArr[1]);
        var staticHour=stringStaticHour.getDigitBanglaFromEnglish();
        var staticMinute=stringStaticMinute.getDigitBanglaFromEnglish();

        // display countdown timer values
        document.getElementById("time-show-4").innerHTML =  convertedHour + " ঘ: " + convertedMinute + " মি: " + convertedSecond + " সে:";
        document.getElementById("iftar-time-4").innerHTML = staticHour + " : " + staticMinute + " মিনিট ";
        // when countdown ends
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("time-show-4").innerHTML = "এখন ইফতার!";
        }
    }, 1000);


}); // document ready ends
 